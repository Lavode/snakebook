# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions
# of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

# Todo: Adding an Unsigned book, saving, then loading it again, produces a rather ugly error. No clue why so far.
# Todo: Turns out it doesn't do that now, seems to be either a silly issue or some hard-to-find issue.

import logging
from operator import attrgetter

from pynbt.nbt import NBTFile, TAG_Byte, TAG_Short, TAG_List, TAG_Compound, TAG_String


class InventoryManager(object):
    def __init__(self):
        self._nbtfile = None
        self._available_inventory_slots = None
        self.inventory = []

        self._update_available_inventory_slots()


    def _update_available_inventory_slots(self):
        # Creates a list containing every possible inventory slot. Will be used to check whether there's space for a new book.
        self._available_inventory_slots = [slot for slot in range(36)]
        # Iterates over all the items in the player's inventory, removing the used inventory slots.
        for item in self.inventory:
            try:
                self._available_inventory_slots.remove(item.slot)
            except ValueError:
                # Seems the slot which we tried to remove isn't present.
                logging.warning("inventorymanager/_update_available_inventory_slots: Slot {0} not present in {1}.".format(item.slot, self._available_inventory_slots))

    def _sort_inventory(self, key="slot"):
        self.inventory.sort(key=attrgetter(key))


    def read_from_nbt(self, path):
        try:
            self.inventory = []
            with open(path, mode="rb") as reader:
                self._nbtfile = NBTFile(reader, compression=NBTFile.Compression.GZIP)

            for item_entry in self._nbtfile["Data"]["Player"]["Inventory"]:
                item_id = item_entry["id"].value
                if item_id == 386:
                    logging.debug("Unsigned book found.")
                    book = UnsignedBook()
                    book.read_from_dictionary(item_entry)
                    self.inventory.append(book)
                    logging.debug("Unsigned book added.")
                elif item_id == 387:
                    logging.debug("Signed book found.")
                    book = SignedBook()
                    book.read_from_dictionary(item_entry)
                    self.inventory.append(book)
                    logging.debug("Signed book added.")
                else:
                    logging.debug("Non-book item with ID {0} found".format(item_id))
                    item = Item()
                    item.read_from_dictionary(item_entry)
                    self.inventory.append(item)
                    logging.debug("Item added")

        except IOError:
            logging.exception("Bookworm/read_from_nbt: The file did not exist or you are lacking permission to read it. Aborting")
            return False

        else:
            self._sort_inventory()
            self._update_available_inventory_slots()
            return True

    def write_to_nbt(self, path):
        try:
            with open(path, mode="wb") as writer:
                self._nbtfile.save(writer, compression=NBTFile.Compression.GZIP)

        except IOError:
            logging.exception("Bookworm/write_to_nbt: Could not write to file. Do you have sufficient permission?")
            return False

        else:
            return True

    def save(self, path):
        return self.write_to_nbt(path)


    def add_item(self, item):
        if item in self.inventory:
            # Trying to add the same object will lead to the following behaviour:
            # New object with slot 1 gets added to the list
            # Same objects gets added to the list, its slot (since 1 is taken already) gets changed to 35
            # Since both names point to the same object, the first list entry's slot gets changed to 35 as well
            # Now there are two entries with slot 35, which leads to an issue when trying to find out which slots are taken already.
            # Therefore it doesn't allow you to add the same item twice.
            return False
        if item.slot not in self._available_inventory_slots:
            try:
                logging.warning("Slot {0} is already in use.".format(item.slot))
                item.slot = self._available_inventory_slots.pop()
            except IndexError:
                # Whoops, no more slots left it seems.
                logging.warning("No more empty inventory slots left! Item will not be added.")
                return False
            else:
                logging.warning("Empty inventory slot found, using slot {0} now.".format(item.slot))

        self.inventory.append(item)
        self._add_item_to_nbt(item)
        self._sort_inventory()
        self._update_available_inventory_slots()
        return True

    def _add_item_to_nbt(self, item):
        self._nbtfile["Data"]["Player"]["Inventory"].append(item.write_to_dictionary())


    def remove_item(self, item):
        return self.remove_item_by_slot(item.slot)

    def remove_item_by_slot(self, slot):
        #print("Slot to remove:", slot)
        for item in self.inventory:
            #print("Slot of current item: ", item.slot)
            if item.slot == slot:
                #print("Match found!")
                self.inventory.remove(item)
                self._remove_item_from_nbt(slot)
                self._sort_inventory()
                self._update_available_inventory_slots()
                return True
        return False

    def _remove_item_from_nbt(self, slot):
        for item in self._nbtfile["Data"]["Player"]["Inventory"]:
            if item["Slot"].value == slot:
                self._nbtfile["Data"]["Player"]["Inventory"].remove(item)
                return True


    def replace_item(self, slot, item):
        # Todo: Doesn't work properly yet, too tired though.
        # Todo: Seemed to work just fine
        self.remove_item_by_slot(slot)
        self.add_item(item)



class Item(object):
    def __init__(self, count=None, slot=1, damage=None, id=None):
        self.count = count
        self.slot = slot
        self.damage = damage
        self.id = id

    def read_from_dictionary(self, d):
        logging.debug("Item/read_from_dictionary: Creating an item with the following dictionary:\n{0}".format(d.pretty()))
        self.count = d["Count"].value
        self.slot = d["Slot"].value
        self.damage = d["Damage"].value
        self.id = d["id"].value

    def write_to_dictionary(self):
        d = {"Count": TAG_Byte(self.count),
             "Slot": TAG_Byte(self.slot),
             "id": TAG_Short(self.id),
             "Damage": TAG_Short(self.damage)}

        return d



class Book(Item):
    def __init__(self, pages=None, slot=0, id=None):
        Item.__init__(self, count=1, slot=slot, damage=0, id=id)
        if pages is not None:
            try:
                self.pages = pages
            except ValueError:
                logging.warning("Book/__init__: pages was not iterable, using an empty list instead")
                self.pages = []
        else:
            self.pages = []

        self.slot = slot

    def read_from_dictionary(self, d):
        logging.debug("Book/read_from_dictionary: Creating book with the following dictionary:\n{0}".format(d.pretty()))

        self.slot = d["Slot"].value

        try:
            for page in d["tag"]["pages"]:
                self.pages.append(page.value)
        except KeyError:
            logging.warning("Book/read_from_dictionary: No pages key in dictionary, result will be empty!")

    def is_signed(self):
        if self.id == 386:
            return False
        elif self.id == 387:
            return True


class SignedBook(Book):
    id = 387
    def __init__(self, author=None, title=None, pages=None):
        Book.__init__(self, pages, id=SignedBook.id)
        self.author = author
        self.title = title

    def __repr__(self):
        return self.pretty()

    def read_from_dictionary(self, d):
        logging.debug("SignedBook/read_from_dictionary: Creating book with the following dictionary:\n{0}".format(d.pretty()))

        self.slot = d["Slot"].value

        try:
            for page in d["tag"]["pages"]:
                self.pages.append(page.value)
        except KeyError:
            logging.warning("SignedBook/read_from_dictionary: No pages key in dictionary, pages will be empty!")

        try:
            self.author = d["tag"]["author"].value
        except KeyError:
            logging.warning("SignedBook/read_from_dictionary: No author key in dictionary, author will be empty!")

        try:
            self.title = d["tag"]["title"].value
        except KeyError:
            logging.warning("SignedBook/read_from_dictionary: No title key in dictionary, title will be empty!")

    def write_to_dictionary(self):
        d = {"Count": TAG_Byte(1),
             "Slot": TAG_Byte(self.slot),
             "tag": TAG_Compound(
                     {"author": TAG_String(self.author),
                      "title": TAG_String(self.title),
                      "pages": TAG_List(TAG_String, self.pages)}),
             "id": TAG_Short(SignedBook.id),
             "Damage": TAG_Short(0)}

        return d

    def pretty(self):
        slist = []
        slist.append("Signed book with ID {0}".format(SignedBook.id))
        slist.append("Inventory slot: {0}".format(self.slot))
        slist.append("Author: {0}".format(self.author))
        slist.append("Title: {0}".format(self.title))
        slist.append("Page(s):")
        slist.extend(["- {0}".format(page) for page in self.pages])
        return "\n".join(slist)

    def get_title(self, length=30, formatting_string="{0} (by {1}): {2}"):
        full_string = formatting_string.format(self.title, self.author, " ".join([page for page in self.pages]))

        if len(full_string) <= length:
            return full_string[0:length]
        else:
            return (full_string[0:length-3] + "...")

    def to_unsigned(self):
        book = UnsignedBook()
        book.pages = self.pages
        book.slot = self.slot

        return book


class UnsignedBook(Book):
    id = 386
    def __init__(self, pages=None):
        Book.__init__(self, pages, id=UnsignedBook.id)

    def __repr__(self):
        return self.pretty()

    def write_to_dictionary(self):
        d = {"Count": TAG_Byte(1),
             "Slot": TAG_Byte(self.slot),
             "tag": TAG_Compound(
                     {"pages": TAG_List(TAG_String, self.pages)}),
             "id": TAG_Short(UnsignedBook.id),
             "Damage": TAG_Short(0)}

        return d

    def pretty(self):
        slist = []
        slist.append("Unsigned book with ID {0}".format(UnsignedBook.id))
        slist.append("Inventory slot: {0}".format(self.slot))
        slist.append("Page(s):")
        slist.extend(["- {0}".format(page) for page in self.pages])
        return "\n".join(slist)

    def get_title(self, length=30, formatting_string="{0}"):
        full_string = formatting_string.format(" ".join([page for page in self.pages]))

        if len(full_string) <= length:
            return full_string[0:length]
        else:
            return (full_string[0:length-3] + "...")

    def to_signed(self, author=None, title=None):
        book = SignedBook()
        book.pages = self.pages
        book.author = author
        book.title = title
        book.slot = self.slot

        return book


