# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions
# of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
# Todo: The pretty() functions of SignedBook and UnsignedBook could be using string formatting instead of joining a list of strings.

# Todo: Logging if removing / adding a book failed might be useful.

from inventorymanager import Item, InventoryManager, SignedBook, UnsignedBook

import logging

class Library(object):
    def __init__(self, path=None):
        self._path = path
        self._manager = InventoryManager()
        self._bookshelf = []

    def _get_bookshelf(self):
        return self._bookshelf

    bookshelf = property(_get_bookshelf)


    def _reload_inventory(self):
        del self._bookshelf[:]
        self._bookshelf.extend([item for item in self._manager.inventory if item.id in [386,387]])


    def read(self, path=None):
        if path is None and self._path is None:
            return False
        elif path is None:
            path = self._path

        success = self._manager.read_from_nbt(path)
        self._reload_inventory()
        return success

    def save(self, path=None):
        if path is None and self._path is None:
            return False
        elif path is None:
            path = self._path

        success = self._manager.write_to_nbt(path)
        return success


    def add_book(self, book):
        success = self._manager.add_item(book)
        self._reload_inventory()

        if not success:
            logging.warning("Failed to add book {0}}".format(book.pretty()))
        return success


    def remove_book(self, book):
        return self.remove_book_by_slot(book.slot)

    def remove_book_by_slot(self, slot):
        success = self._manager.remove_item_by_slot(slot)
        self._reload_inventory()

        if not success:
            logging.warning("Failed to remove book at slot {0}".format(slot))

        return success


    def replace_book_by_slot(self, slot, book_to_add):
        success = self.remove_book_by_slot(slot)
        # If removing the book failed, bailing out.
        if not success:
            return False

        book_to_add.slot = slot
        return self.add_book(book_to_add)

    def replace_book(self, book_to_remove, book_to_add):
        self.replace_book_by_slot(book_to_remove.slot, book_to_add)


    def make_signed(self, book):
        signed_book = book.to_signed()
        success = self.remove_book_by_slot(book.slot)
        # If removing the book failed, bailing out.
        if not success:
            return False

        return self.add_book(signed_book)

    def make_unsigned(self, book):
        unsigned_book = book.to_unsigned()
        success = self.remove_book_by_slot(book.slot)
        # If removing the book failed, bailing out.
        if not success:
            return False

        return self.add_book(unsigned_book)







